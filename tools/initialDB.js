const fs = require("fs");

if (!fs.existsSync("../data"))
    fs.mkdirSync("../data");

const knex = require('knex')({
    client: 'sqlite3',
    connection: {
        filename: '../data/data.db',
    },
    debug: false,
    useNullAsDefault: true,
});

knex.schema.createTable('devices', table => {
    table.bigInteger('id').notNullable();
    table.string('type');
    table.string('sn');
    table.string('power');
    table.string('state');
    table.string('time');
}).then(() => {
    console.log("\n数据库初始化完毕！\n\n按Ctrl + c退出\n")
}).catch(err => {
    // console.error(err)
    // return err;
    console.error("\n数据库已存在，请勿重复初始化！\n\n按Ctrl + c退出\n")
})