const CONF = {
    //服务器地址
    host: "127.0.0.1",
    //web服务端口
    webPort: 80,
    //socket server端口
    socketPort: 8023,
}
module.exports = CONF;