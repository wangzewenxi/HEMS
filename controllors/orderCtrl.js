const dao = require("../DAO/ordersDAO")

class orders {
    static async get(ctx) {
        var data = await dao.getOrders().then((value) => {
            return value
        })
        ctx.type = "application/json";
        ctx.body = data;
    }
}
module.exports = orders