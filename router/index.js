const router = require("@koa/router");
const route = new router();
const orderCtrl = require("../controllors/orderCtrl")
const chartCtrl = require("../controllors/chartCtrl")
const server = require("../controllors/hemsSocket");

route.get("/getErrorReports", orderCtrl.get)
route.get("/getRealtimeReports", chartCtrl.get)
// route.get("/control", async ctx => {
//     // console.log(action)
//     switch (ctx.query.action) {
//         case "reboot":
//             server.listen(conf.socketPort, conf.host, () => {
//                 console.log("socket服务已启动", server.address());
//             });
//             break;
//         case "shutdown":
//             server.close(err => {
//                 console.error(err);
//             });
//             break;
//         default:
//             break;
//     }
//     ctx.body = "already shutdown"
// })

module.exports = route;