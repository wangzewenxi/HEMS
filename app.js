const koa = require("koa2");
const app = new koa();
const route = require("./router");
const koastatic = require("koa-static");
const conf = require("./config");
const fs = require("fs");


// console.log(fs.readFileSync("./config.js", "UTF-8").split("module")[0]);
fs.writeFileSync("./resource/config.js", fs.readFileSync("./config.js", "UTF-8").split("module")[0])


app.use(koastatic("./resource"));
app.use(route.routes()).use(route.allowedMethods());

app.listen(conf.webPort, conf.host, () => {
    console.log("智能家居设备监控WEB面板 - http://" + conf.host + ":" + conf.webPort);
})