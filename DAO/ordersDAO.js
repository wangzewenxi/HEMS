const knex = require("./conn")

class ordersDAO {
    static async getOrders() {
        var data = await knex('devices').select('rowid', 'id', 'sn', 'power', 'state', 'time')
            .whereRaw('rowid%10=?', 1)
            .orderBy('time', "desc")
            .limit(10)
            .then((row) => {
                return row
            }).catch((err) => {
                console.error(err);
            })
        return data;
    }
}

module.exports = ordersDAO