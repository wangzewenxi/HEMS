const knex = require("knex")({
    client: "sqlite3",
    connection: {
        filename: "./data/data.db"
    },
    debug: false,
    useNullAsDefault: true,
})

module.exports = knex;