const knex = require("./conn");

class chartDAO {
    static async getChart(id) {
        var data = await knex('devices').select('rowid', 'id', 'sn', 'power', 'state', 'time')
            .where("id","=",id)
            .orderBy('time', "desc")
            .limit(10)
            .then((row) => {
                return row
            }).catch((err) => {
                console.error(err);
            })
        return data;
    }
}

module.exports = chartDAO